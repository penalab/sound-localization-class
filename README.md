# Sound Localization - class room self-experience

This web app features multiple "exercises" to experience sensory phenomena linked to sound locatlization.

It can be downloaded and used _offline_ (open `index.html` in browser), uploaded to your own webserver or, as of now, accessed at:

https://tyto.xyz/class

The Jupyter Notebook file `Create Files.ipynb` contains the Python code to create included sound files (`.wav`). If you create new/different sound files, you may have to update the JavaScript `exercises` variable in `index.html` accordingly.

## Author

Roland Ferger, roland.ferger@einsteinmed.org

## License - CC BY-SA 4.0

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Sound Localization - class room self-experience</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Roland Ferger</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
